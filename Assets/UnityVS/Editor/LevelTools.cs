﻿using UnityEngine;
using System.Collections;

using UnityEditor;
using System;
using UnityEditor.VersionControl;
using Object = UnityEngine.Object;

public class LevelTools : MonoBehaviour
{
    #region Create the outer walls.

    private static Transform parentForBedrock = GameObject.Find("Base Walls").transform;
    static Object stonePrefab = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Stone Prefab.prefab", typeof(GameObject));
    static Object stoneWallPrefab = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Stone Wall Prefab.prefab", typeof(GameObject));
    static GameObject go;
    [MenuItem("Level Creator/Create BedRock")]
    static void CreateFloor()
    {
        for (int x = 0; x <= 25; x++)
        {
            if (x == 0 || x == 25)
            {
                makeWalls(x);
                go = Instantiate(stoneWallPrefab) as GameObject;
            }
            else
                go = Instantiate(stonePrefab) as GameObject;

            go.transform.SetParent(parentForBedrock);
            go.transform.position = new Vector3(x, 0, 0);
            go.name = "StoneMid:" + go.transform.position;
            go.tag = "Ground";
        }        
    }

    private static void makeWalls(int x)
    {
        for (int i = 1; i <= 13; i++)
        {
            if (i == 13 && x != 25)
                makeRoof(i);

            go = Instantiate(stoneWallPrefab) as GameObject;
            go.transform.SetParent(parentForBedrock);
            go.transform.position = new Vector3(x, i, 0);
            go.name = "StoneMid:" + go.transform.position;
            go.tag = "Ground";
        }
    }

    private static void makeRoof(int i)
    {
        for (int x = 1; x <= 24; x++)
        {
            //if (x == 1 || x == 24)
            //{
            //    go = Instantiate(stoneWallPrefab) as GameObject;
            //}
            //else
                go = Instantiate(stonePrefab) as GameObject;

            go.transform.SetParent(parentForBedrock);
            go.transform.position = new Vector3(x, i, 0);
            go.transform.rotation = new Quaternion(180,0,0,0);
            go.name = "StoneMid:" + go.transform.position;
            go.tag = "Ground";
        }
    }
    #endregion

    #region Dirt ground.

    private static Object dirtPrefab = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Dirt Prefab.prefab", typeof (GameObject));
    private static Transform parentForGrass = GameObject.Find("Level").transform;
    [MenuItem("Level Creator/Create Dirt ground")]
    static void CreateGround()
    {
        for (int i = 1; i <= 24; i++)
        {
            go = Instantiate(dirtPrefab) as GameObject;
            go.transform.SetParent(parentForGrass);
            go.transform.position = new Vector3(i, 1, 0);
            go.name = "GrassMid:" + go.transform.position;
            go.tag = "Ground";
        }
    }

    #endregion
}
