﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using UnityEngine.EventSystems;

public class PlayerControls : MonoBehaviour
{
    SpriteRenderer player_Sr;
    private Animator player_AnimationCast;

    // Use this for initialization
    void Start()
    {
        player_Sr = GetComponent<SpriteRenderer>();
        player_AnimationCast = GetComponent<Animator>();
        player_Sr.flipX = true;
        timeDelay = Time.time + 0.7f;
        jumpDelay = Time.time + 0.2f;
        savedSpeed = Speed;
        drowningDelay = Time.time;
    }

    Rigidbody2D player_rb2d;
    // Update is called once per frame
    public float Speed = 50f;
    public float JumpSpeed = 0.1f;
    private float movex;
    private bool isJumping;
    private float timeDelay;
    private float jumpDelay;
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded && (Time.time >= jumpDelay))
        {
            jumpDelay = Time.time + 0.2f;
            isJumping = true;
            //Debug.Log("jump");
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            isJumping = false;
        }
        if (Input.GetButton("Fire1") && (Time.time >= timeDelay) && !EventSystem.current.IsPointerOverGameObject())
        {
            timeDelay = Time.time + 0.7f;
            player_AnimationCast.SetTrigger("IsCasting");

            //StartCoroutine(CastingDelay());
            //Debug.Log("It sohuld be casting");
        }
        if (drowningTime <= Time.time && isDrowning)
            beginDrowning();
        checkGround();
    }
    bool isDrowning;
    private float drowningDelay;
    private void beginDrowning()
    {
        if (drowningDelay <= Time.time)
        {
            drowningDelay = Time.time + 1f;
            GetComponent<Player>().HitPoints -= 5;
        }
    }

    bool IsCastingDelay;
    /// <summary>
    /// Is the casting delay so the whole animation is played 
    /// </summary>
    /// <returns></returns>
    IEnumerator CastingDelay()
    {
        if (IsCastingDelay)
            yield break;
        IsCastingDelay = true;
        player_AnimationCast.SetBool("IsCastingMagic", true);
        //Debug.Log("It should wait 0.1 second");
        yield return new WaitForSeconds(0.37f);
        player_AnimationCast.SetBool("IsCastingMagic", false);
        IsCastingDelay = false;
    }

    void FixedUpdate()
    {
        movex = Input.GetAxisRaw("Horizontal");

        if (movex < 0)
        {
            player_Sr.flipX = false;
        }
        if (movex >= 1)
        {
            player_Sr.flipX = true;
        }

        player_AnimationCast.SetBool("IsRunning", true);
        if (player_rb2d == null)
            player_rb2d = GetComponent<Rigidbody2D>();

        var movementSpeed = movex * Speed;
        player_rb2d.AddForce(new Vector2(movementSpeed, 0));

        if (!Input.GetButton("Running"))
            player_AnimationCast.SetBool("IsRunning", false);

        if (isJumping)
        {            
            player_rb2d.AddForce(new Vector2(movex * JumpSpeed, 15f), ForceMode2D.Impulse);
            player_rb2d.AddForce(new Vector2(movex * JumpSpeed, 7.5f), ForceMode2D.Impulse);
            GetComponent<Rigidbody2D>().gravityScale = 6;
            isJumping = false;
            isGrounded = false;
        }

    }

    private void checkGround()
    {
        int x = 1;
        var som = Physics2D.RaycastAll(transform.position, Vector2.down);
        if (som[x].transform.name.Contains("Player"))
            x = 2;
        var sum = transform.position - som[x].transform.position;
        if (sum.y <= 1.1f && !som[x].transform.name.Contains("Water"))
        {
            isGrounded = true;
            GetComponent<Rigidbody2D>().gravityScale = 3;
        }
    }

    bool isGrounded = true;
    void OnCollisionEnter2D(Collision2D coll)
    {       
        //Debug.Log("Made it");
        //checkGround();
        //if (coll.transform.tag.Contains("Ground"))
        //{
        //    Debug.Log("IsGrounded = true");
        //    isGrounded = true;
        //    lastGroundPosY = transform.position.y;
        //}
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        //checkGround();
        //if (coll.transform.tag.Contains("Ground"))
        //{
        //    Debug.Log("IsGrounded = false");
        //    isGrounded = false;
        //}
    }

    private float drowningTime;
    private float savedSpeed;
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.name.Contains("Water"))
        {
            if (Speed == savedSpeed)
            {
                drowningTime = Time.time + 7.5f;
                isDrowning = true;
                Speed /= 2;
            }
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.name.Contains("Water"))
        {
            if (Speed != savedSpeed)
            {
                isDrowning = false;
                Speed *= 2;
            }
        }
    }
}
