﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{
    private Camera camera1;
	// Use this for initialization
	void Start ()
    {
        camera1 = Camera.main;
    }
    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;
    private Transform target;
    public float Height = 5f;
    // Update is called once per frame
    void Update()
    {
        if(target == null)
            target = GameObject.Find("Player").transform;
        if (target)
        {
            Vector3 point = camera1.WorldToViewportPoint(target.position);
            Vector3 delta = target.position - camera1.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));
            Vector3 destination = transform.position + delta;
            destination.y += Height;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
            
        }

    }
    //Working. I would like a smoother movement so if all else fails, this will still work.

    //void Update()
    //{
    //    if (player == null)
    //        player = GameObject.Find("Player");
    //    var tempPos = player.transform.position;
    //    tempPos.z = -10;
    //    main.transform.position = tempPos;

    //}
}
