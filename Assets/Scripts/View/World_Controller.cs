﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Model;

public class World_Controller : MonoBehaviour
{
    public Sprite backgroundImage;
    public Player Player { get; set; }
    public DamageSystem damageSystem;
    // Use this for initialization
    void Start()
    {
        while (Player == null && Averium == null)
        {
            Player = GameObject.Find("Player").GetComponent<Player>();
            //if (Player != null)
            //{
                damageSystem = new DamageSystem(Player);
                damageSystem.GameOverEventHandler += GameOver;
                Averium = GetComponent<World_Controller>().Player.Averium;
                //if(Averium != null)
                    Averium.MagicAddedEvent += onChangedMagicBook;
            //}
        }
    }

    private void GameOver(object sender, GameOverArgs e)
    {
        //Do some game over thingy here.
    }

    private void onChangedMagicBook(object e, MagicChangedArgs args)
    {
        //SendMessage doesnt work as of yet. Maybe it gets removed, maybe not.
        if (args.Message.Contains("MagicBookChanged"))
        {
            Averium = e as MagicBook;
            //SendMessage("TheMagicBook", e);            
        }
        //else if (args.Message.Contains("MagicChanged"))
        //{
        //    SendMessage("CurrentMagic", e);
        //}
    }

    public MagicBook Averium { get; private set; }

    // Update is called once per frame
    void Update()
    {

    }

    private void GameStart()
    {
        GameObject backGround = new GameObject("Platform_Ground");
        var background_SR = backGround.AddComponent<SpriteRenderer>();
        background_SR.sprite = backgroundImage;
        background_SR.transform.localScale = new Vector3(25, 25, 0);
        var background_BoxCol2d = backGround.AddComponent<BoxCollider2D>();
        background_BoxCol2d.size = new Vector2(1, 0.03006031f);
        background_BoxCol2d.offset = new Vector2(0, -0.228f);
        background_BoxCol2d.sharedMaterial = new PhysicsMaterial2D("Std.Platform_Material");

    }
}
