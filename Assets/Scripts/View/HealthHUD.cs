﻿using System;
using UnityEngine;
using System.Collections;

public class HealthHUD : MonoBehaviour
{
    public Sprite FullHeart;
    public Sprite HalfHeart;
    public Sprite EmptyHeart;

    public GameObject Heart1;
    public GameObject Heart2;
    public GameObject Heart3;
    public GameObject Heart4;
    public GameObject Heart5;

    private Player player;
    // Use this for initialization
    void Start()
    {
        while (player == null)
        {
            player = GameObject.Find("World Keeper").GetComponent<World_Controller>().Player;
            //if(player != null)
                player.HitPointsEventHandler += HitPointsEventHandler;
        }
    }

    private void HitPointsEventHandler(object sender, HitPointArgs hitPointArgs)
    {
        setLayoutHearts();
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            player = GameObject.Find("World Keeper").GetComponent<World_Controller>().Player;
            player.HitPointsEventHandler += HitPointsEventHandler;
        }
    }
    /// <summary>
    /// Changes the players heart based on how much health the player have.
    /// </summary>
        #region HeartLayout      
    private void setLayoutHearts()
    {
        var hp = player.HitPoints;
        Debug.Log(hp);
        if (100 >= hp && 91 <= hp)
        {
            Heart5.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart4.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart3.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart2.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart1.GetComponent<SpriteRenderer>().sprite = FullHeart;
            return;
        }
        if (90 >= hp && 81 <= hp)
        {
            Heart5.GetComponent<SpriteRenderer>().sprite = HalfHeart;
            Heart4.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart3.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart2.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart1.GetComponent<SpriteRenderer>().sprite = FullHeart;
            return;
        }
        if (80 >= hp && 71 <= hp)
        {
            Heart5.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart4.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart3.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart2.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart1.GetComponent<SpriteRenderer>().sprite = FullHeart;
            return;
        }
        if (70 >= hp && 61 <= hp)
        {
            Heart5.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart4.GetComponent<SpriteRenderer>().sprite = HalfHeart;
            Heart3.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart2.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart1.GetComponent<SpriteRenderer>().sprite = FullHeart;
            return;
        }
        if (60 >= hp && 51 <= hp)
        {
            Heart5.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart4.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart3.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart2.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart1.GetComponent<SpriteRenderer>().sprite = FullHeart;
            return;
        }
        if (50 >= hp && 41 <= hp)
        {
            Heart5.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart4.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart3.GetComponent<SpriteRenderer>().sprite = HalfHeart;
            Heart2.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart1.GetComponent<SpriteRenderer>().sprite = FullHeart;
            return;
        }
        if (40 >= hp && 31 <= hp)
        {
            Heart5.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart4.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart3.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart2.GetComponent<SpriteRenderer>().sprite = FullHeart;
            Heart1.GetComponent<SpriteRenderer>().sprite = FullHeart;
            return;
        }
        if (30 >= hp && 21 <= hp)
        {
            Heart5.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart4.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart3.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart2.GetComponent<SpriteRenderer>().sprite = HalfHeart;
            Heart1.GetComponent<SpriteRenderer>().sprite = FullHeart;
            return;
        }
        if (20 >= hp && 11 <= hp)
        {
            Heart5.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart4.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart3.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart2.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart1.GetComponent<SpriteRenderer>().sprite = FullHeart;
            return;
        }
        if (10 >= hp && 1 <= hp)
        {
            Heart5.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart4.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart3.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart2.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart1.GetComponent<SpriteRenderer>().sprite = HalfHeart;
            return;
        }
        if (0 >= hp)
        {
            Heart5.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart4.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart3.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart2.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            Heart1.GetComponent<SpriteRenderer>().sprite = EmptyHeart;
            return;
        }
    }
    #endregion

}
