﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
//TODO Currently there's a lot of problems with the fluid. It is harder than I first anticipated

//TODO First. The LevelKeeper has to keep track of all the current tiles in the level.

//TODO Second. It should see if there's a body of water or more in the level, and keep track of them

//TODO Third. It should keep track of the actual tiles holding the water together

//TODO Fourth. It should subscribe to the destruction events for the different tiles.

//TODO Fifth. When one of the tiles is destroyed it should check and see if one of them belongs to the third step.

//TODO Sixth. It should then move the highest and closest tile to the new position

//TODO Seventh.Repeat sixth untill the water is level.


public class OLDDONOTUSELevelKeeper : MonoBehaviour
{
    public GameObject WaterTile;

    private Dictionary<string, Transform> waterTiles;
    private Dictionary<string, Transform> edgesOfWater;
    private Dictionary<string, Transform> bucketForWater;
    /// <summary>
    /// Contains all the current tiles in the level
    /// </summary>
    private Dictionary<string, Transform> tileList;
    private SortedDictionary<int, List<int>> squareDictionary;
    private float biggestTileY;
    private float biggestTileX;
    private float smallestTileY;
    private float smallestTileX;
    private int bucketForWaterCount;

    // Use this for initialization
    void Start()
    {
        tileList = new Dictionary<string, Transform>();
        waterTiles = new Dictionary<string, Transform>();
        edgesOfWater = new Dictionary<string, Transform>();
        bucketForWater = new Dictionary<string, Transform>();

        foreach (var tile in GetComponentsInChildren<Transform>())
        {
            if (tile.name.Contains(":("))
            {
                if (tileList.ContainsKey(tile.name.Split(':')[1]))
                    Debug.Log("This tile already exists: " + tile);
                else
                {
                    tileList.Add(tile.name.Split(':')[1], tile);
                }
            }
        }
        setWaterTileList();
        findEdgesOfTile(waterTiles);
        setXYValues();
        findBucketForWater();
    }

    /// <summary>
    /// Call this from a water tile to check if it is destroyable.
    /// </summary>
    /// <param name="waterTileTransform">Is the transform from the water tile</param>
    /// <returns>a bool based on if it can be destroyed. True = Destroyable</returns>
    public bool CanWaterTileBeDestroyed(Transform waterTileTransform)
    {
        var vector3 = waterTileTransform.position;
        if (Mathf.FloorToInt(vector3.y) != Mathf.FloorToInt(biggestTileY))
            return false;

        var vector3AsString = vector3.ToString();
        foreach (var tile in edgesOfWater)
        {
            if (tile.Key.Contains(vector3AsString))
            {
                return true;
            }
        }
        return false;
    }
    // DO NOT CHANGE THIS VALUE. It have been calculated beforehand. It is a^2 + b^2 = c^2 => sqr(c^2) = radius of the needed circle to do the CircleCastAll
    private float radiusOfCircleCast = 0.7071006f;

    /// <summary>
    /// Simulates the water with a little help from the destroyed tile.
    /// </summary>
    private void waterSimulation(Transform destroyedTile)
    {
        if (!isThereABucket())
        {
            var waterTile_go = makeWaterTile(destroyedTile.position);
            var som = Physics2D.CircleCastAll(waterTile_go.transform.position, radiusOfCircleCast, Vector2.zero);

            var sum = som.Where(ray => ray.transform.tag.Contains("Magic")).Aggregate(som.Length, (current, ray) => current - 1);

            Debug.Log(sum.ToString());
            switch (sum)
            {
                case 5:
                    return;
                case 4:
                    //TODO: MAKE THIS WORK. IT WILL CRASH IF RUN.
                    //For some unknown reason, it's not assigning anything to the currentWaterTileBeingWorkedOn after the initial set up a little higher up. Might have something to do with coroutines
                    
                    break;
                case 3:

                    break;
            }
        }
    }

    private bool isThereABucket()
    {
        throw new NotImplementedException();
    }

    private GameObject currentWaterTileBeingWorkedOn;
    private IEnumerator removeTileFurthestAway(Vector3 tempDirection)
    {
        yield return new WaitForSeconds(0.2f);
        Transform biggestDistanceTile = edgesOfWater.FirstOrDefault().Value;
        float oldDistance = 0;
        foreach (var tile in edgesOfWater)
        {
            var distance = Vector3.Distance(tempDirection, tile.Value.position);
            if (distance >= oldDistance)
            {
                oldDistance = distance;
                biggestDistanceTile = tile.Value;
            }
        }
        Destroy(biggestDistanceTile.gameObject);
    }

    private Vector3 directionOfMissingTileX(Vector3 position)
    {
        var tempPosition = position;
        tempPosition.x += 1;
        var searchString = tempPosition.ToString();
        if (!searchTileList(searchString))
        {
            return tempPosition;
        }
        var tempPosition2 = position;
        tempPosition2.x -= 1;
        var searchString2 = tempPosition2.ToString();
        if (!searchTileList(searchString2))
        {
            return tempPosition2;
        }

        return Vector3.zero;
    }
    private bool searchTileList(string position)
    {
        if (tileList.ContainsKey(position))
            return true;
        return false;
    }
    private GameObject makeWaterTile(Vector3 position, int stage = 0)
    {
        GameObject waterTile_go = Instantiate(WaterTile);
        waterTile_go.transform.SetParent(transform);
        waterTile_go.GetComponent<Transform>().Translate(position);
        waterTile_go.GetComponent<WaterProperty>().SpriteStartNr = stage;
        addItemToDictionary(tileList, waterTile_go.transform);
        addItemToDictionary(waterTiles, waterTile_go.transform);
        currentWaterTileBeingWorkedOn = waterTile_go;
        return waterTile_go;
    }
    private IEnumerator makeWaterTileDelay(Vector3 position, int stage = 0)
    {
        yield return new WaitForSeconds(0.2f);
        makeWaterTile(position, stage);
    }
    /// <summary>
    /// Simulates the water. Aka. when a water tile is destroyed it should settle all the other nearby water tiles.
    /// </summary>
    private void waterSimulation()
    {
        if (bucketForWater.Count != bucketForWaterCount)
        {

        }
    }
    /// <summary>
    /// Call this to check if a body of water is a rectangle. 
    /// </summary>
    /// <returns>a bool based on if it a rectangle. True = Rectangle</returns>
    private bool isItRectangle()
    {
        bool isItRectangle = false;
        // What happens here is that we need to compare 2 different X's here and their Y values.
        foreach (var kvpOri in squareDictionary)
        {

            var tempKvp = kvpOri;
            foreach (var kvp in squareDictionary)
            {
                if (tempKvp.Key != kvp.Key)
                {
                    //Here the X values are checked for if they are a cont. of each other
                    if (((tempKvp.Key - kvp.Key) == 1) || ((kvp.Key - tempKvp.Key) == 1))
                    {
                        tempKvp.Value.Sort();
                        kvp.Value.Sort();
                        //If this is true we return immediately because that means there is one or more y in the x's list and that can't be a rectangle
                        if (tempKvp.Value.Count != kvp.Value.Count)
                        {
                            //Debug.Log("tempKvp value count against kvp value count was false");
                            return false;
                        }
                        if (tempKvp.Value.Count == kvp.Value.Count)
                        {
                            //Debug.Log("tempKvp value count against kvp value count was true");
                            for (int i = 0; i < kvp.Value.Count; i++)
                            {
                                // Here is where the real magic happens. We substract x1 : y1 with x2 : y1 and so on. Every one of these NEEDS to be zero because that means they are equal.
                                //Debug.Log("Checking to see if the values in the kvp list are the same");
                                if ((tempKvp.Value[i] - kvp.Value[i]) == 0)
                                {
                                    var tempNr = tempKvp.Value[i] - kvp.Value[i];
                                    //Debug.Log("This should be 0. Is it?: " + tempNr);
                                    isItRectangle = true;
                                }
                            }

                        }
                    }
                }
                //----------------------------------------
                //Just a log thing. Remove at some point.
                //string log = kvp.Key + ":";
                //foreach (var y in kvp.Value)
                //{
                //    log += y + ",";
                //}
                //Debug.Log(log);
                // ---------------------------------------
            }
        }

        return isItRectangle;
    }
    /// <summary>
    /// Collects and sort through all the water tiles, pairing it up in x1 : y1,y2,y3,y4. x2 : y1,y2,y3,y4
    /// When you want to check and see if it's sqaure, "all" you have to is is basically y1-y1, y2-y2 and so on.
    /// </summary>
    private void setXYValues()
    {
        squareDictionary = new SortedDictionary<int, List<int>>();
        foreach (var tile in waterTiles)
        {
            var x = Mathf.FloorToInt(tile.Value.position.x);
            var y = Mathf.FloorToInt(tile.Value.position.y);

            foreach (var kvp in squareDictionary)
            {
                if (kvp.Key == x)
                    kvp.Value.Add(y);
            }
            if (!squareDictionary.ContainsKey(x))
            {
                squareDictionary.Add(x, new List<int> { y });
            }
        }
    }

    private void setWaterTileList()
    {
        foreach (var kvp in tileList)
        {
            if (kvp.Value.name.Contains("Water"))
            {
                waterTiles.Add(kvp.Key, kvp.Value);
                setDestructionEvent(kvp.Value);
            }
        }
    }
    /// <summary>
    /// Finds what should be the square of the water tiles.
    /// </summary>
    private void findEdgesOfTile(Dictionary<string, Transform> tileDictionary)
    {
        biggestTileY = tileDictionary.FirstOrDefault().Value.position.y;
        biggestTileX = tileDictionary.FirstOrDefault().Value.position.x;
        smallestTileY = tileDictionary.FirstOrDefault().Value.position.y;
        smallestTileX = tileDictionary.FirstOrDefault().Value.position.x;
        foreach (var tile in tileDictionary)
        {
            //Should find the biggest value for y
            biggestTileY = findBiggestValue(tile.Value.transform.position.y, biggestTileY);
            //Should find the biggest value for x
            biggestTileX = findBiggestValue(tile.Value.transform.position.x, biggestTileX);
            //Should find the smallest value for y
            smallestTileY = findSmallestValue(tile.Value.transform.position.y, smallestTileY);
            //Should find the smallest value for x
            smallestTileX = findSmallestValue(tile.Value.transform.position.x, smallestTileX);
        }
        //Debug.Log(biggestTileX + " The biggest X");
        //Debug.Log(biggestTileY + " The biggest Y");
        //Debug.Log(smallestTileX + " The smallest X");
        //Debug.Log(smallestTileY + " The smallest Y");


        // Should find and add the correct water tiles, so the edges of a collection of water can be used to calculate
        // if the water should move.
        foreach (var kvpTile in tileDictionary)
        {
            addTilesToDicionary(kvpTile.Value.transform.position.y, biggestTileY, kvpTile, edgesOfWater);
            addTilesToDicionary(kvpTile.Value.transform.position.x, biggestTileX, kvpTile, edgesOfWater);
            addTilesToDicionary(kvpTile.Value.transform.position.y, smallestTileY, kvpTile, edgesOfWater);
            addTilesToDicionary(kvpTile.Value.transform.position.x, smallestTileX, kvpTile, edgesOfWater);
        }

        //---------------------------------------
        // Only exist for debugging.
        //foreach (var waterTile in edgesOfWater)
        //{
        //    Debug.Log(waterTile);
        //}
        //---------------------------------------
    }

    /// <summary>
    /// Finds the tiles around the body of water.
    /// </summary>
    private void findBucketForWater()
    {
        foreach (var tile in edgesOfWater)
        {
            if (tile.Key.Contains("(" + biggestTileX))
            {
                var tempSplit = tile.Key.Replace("(", "").Replace(")", "").Split(',');
                var tileX = float.Parse(tempSplit[0]);
                var tileY = float.Parse(tempSplit[1]);
                var tileZ = float.Parse(tempSplit[2]);
                tileX += 1;
                addBucketTile(tileX, tileY, tileZ);
                //switch ("(" + tileX + ", " + tileY + ", " + 0 + ")" )
                //{
                //        case 
                //}
            }
            if (tile.Key.Contains("(" + smallestTileX))
            {
                var tempSplit = tile.Key.Replace("(", "").Replace(")", "").Split(',');
                var tileX = float.Parse(tempSplit[0]);
                var tileY = float.Parse(tempSplit[1]);
                var tileZ = float.Parse(tempSplit[2]);
                tileX -= 1;
                addBucketTile(tileX, tileY, tileZ);
            }
            if (tile.Key.Contains(smallestTileY + ".0, 0.0)"))
            {
                //Debug.Log(smallestTileY);
                //Debug.Log(tile.Value.name);
                var tempSplit = tile.Key.Replace("(", "").Replace(")", "").Split(',');
                var tileX = float.Parse(tempSplit[0]);
                var tileY = float.Parse(tempSplit[1]);
                var tileZ = float.Parse(tempSplit[2]);
                tileY -= 1;
                addBucketTile(tileX, tileY, tileZ);
            }
        }
    }

    private void addBucketTile(float tileX, float tileY, float tileZ)
    {
        var tempKey = "(" + tileX + ".0, " + tileY + ".0, " + tileZ + ".0)";
        //Debug.Log(tempKey);
        if (tileList.ContainsKey(tempKey))
        {
            var tempTile = tileList[tempKey];
            //Debug.Log(tempTile.name);
            addItemToDictionary(bucketForWater, tempTile);
            bucketForWaterCount++;
            //setDestructionEvent2(tempTile);
        }
    }
    //private void setDestructionEvent2(Transform tile)
    //{
    //    if (tile.name.Contains("Water"))
    //    {
    //        detructionEventSetting2(tile.GetComponent<WaterProperty>());
    //    }

    //    if (tile.name.Contains("Grass"))
    //    {
    //        detructionEventSetting2(tile.GetComponent<EarthProperty>());
    //    }
    //    if (tile.name.Contains("Wood"))
    //    {
    //        detructionEventSetting2(tile.GetComponent<WoodProperty>());
    //    }
    //    // UNCOMMENT THIS SECTION WHEN IT HAS BEEN IMPLEMENTED ------------------------------
    //    //if (tile.name.Contains("Ice"))
    //    //{
    //    //    setDetructionEvent(tile.GetComponent<IceProperty>());
    //    //}
    //    //-----------------------------------------------------------------------------------
    //}
    //private void detructionEventSetting2<T>(T t) where T : MyMonoBehaviorPlus
    //{
    //    t.DestructionEvent += onWoodDestruction;
    //}

    private void onWoodDestruction(object sender, TileDestructionArgs e)
    {
        Debug.Log("Wood was destroyed");
        var tile = sender as GameObject;
        tileList.Remove(tile.name.Split(':')[1]);
        waterTiles.Remove(tile.name.Split(':')[1]);
        bucketForWater.Remove(tile.name.Split(':')[1]);
        waterSimulation(tile.transform);
    }

    private bool addItemToDictionary(Dictionary<string, Transform> dictionary, Transform gameObjectToBeAdded)
    {
        if (!dictionary.ContainsKey(gameObjectToBeAdded.position.ToString()))
        {
            dictionary.Add(gameObjectToBeAdded.position.ToString(), gameObjectToBeAdded);
            return true;
        }
        Debug.Log(gameObjectToBeAdded.position + " This tile was already in the dictionary?!");
        return false;
    }
    private void setDestructionEvent(Transform tile)
    {
        if (tile.name.Contains("Water"))
        {
            detructionEventSetting(tile.GetComponent<WaterProperty>());
        }

        if (tile.name.Contains("Grass"))
        {
            detructionEventSetting(tile.GetComponent<EarthProperty>());
        }
        if (tile.name.Contains("Wood"))
        {
            detructionEventSetting(tile.GetComponent<WoodProperty>());
        }
        // UNCOMMENT THIS SECTION WHEN IT HAS BEEN IMPLEMENTED ------------------------------
        //if (tile.name.Contains("Ice"))
        //{
        //    setDetructionEvent(tile.GetComponent<IceProperty>());
        //}
        //-----------------------------------------------------------------------------------
    }
    private void detructionEventSetting<T>(T t) where T : MyMonoBehaviorPlus
    {
        //t.DestructionEvent += onDestruction;
    }

    /// <summary>
    /// Is here because I dont know... Might do something else in the future but it works atm.
    /// </summary>
    /// <param name="value"></param>
    /// <param name="number"></param>
    /// <param name="tile"></param>
    /// <param name="tileDictionary"></param>
    private void addTilesToDicionary(float value, float number, KeyValuePair<string, Transform> tile, Dictionary<string, Transform> tileDictionary)
    {
        if (Mathf.FloorToInt(value) == Mathf.FloorToInt(number))
        {
            if (!tileDictionary.ContainsKey(tile.Key))
            {
                tileDictionary.Add(tile.Key, tile.Value);
                if (tile.Value.name.Contains("Water"))
                    detructionEventSetting(tile.Value.GetComponent<WaterProperty>());
            }
            //else
            //    Debug.Log(waterTile + " Was already in the dictionary");
        }
    }
    /// <summary>
    /// This is called when a water tile is destroyed. That way the level keeper knows when to run the water simulation.
    /// </summary>
    /// <param name="e">The object that was destroyed</param>
    /// <param name="args">The TileDestructionArgs</param>
    private void onDestruction(object e, TileDestructionArgs args)
    {
        var tile = e as GameObject;
        edgesOfWater.Remove(tile.name.Split(':')[1]);
        waterTiles.Remove(tile.name.Split(':')[1]);
        findEdgesOfTile(edgesOfWater);
        setXYValues();
        waterSimulation();
    }

    private static float findSmallestValue(float value, float smallestTileY)
    {
        if (value <= smallestTileY)
            smallestTileY = value;
        return smallestTileY;
    }

    private static float findBiggestValue(float value, float biggestTileY)
    {
        if (value >= biggestTileY)
            biggestTileY = value;
        return biggestTileY;
    }
}

