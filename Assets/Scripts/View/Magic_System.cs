﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class Magic_System : MonoBehaviour
{
    private GameObject player;
    private Player playerFromModel;

    // Use this for initialization
    void Start()
    {
        playerFromModel = GameObject.Find("World Keeper").GetComponent<World_Controller>().Player;
        timeDelay = Time.time + 0.7f;
    }
    float timeDelay;
    // Update is called once per frame
    void Update()
    {
        if (playerFromModel == null)
            playerFromModel = GameObject.Find("World Keeper").GetComponent<World_Controller>().Player;
        if (Input.GetButton("Fire1") && !EventSystem.current.IsPointerOverGameObject())
        {
            if (player == null)
                player = GameObject.Find("Player");

            if (Time.time >= timeDelay)
            {
                var playerPos = player.transform.position;
                timeDelay = Time.time + 0.7f;
                StartCoroutine(makeNewMagic_go(playerPos));
            }
        }
    }
    /// <summary>
    /// Makes the object after a brief waiting period to be in sync with animation
    /// </summary>
    /// <param name="playerPos">Is the current position of the player</param>
    /// <returns></returns>
    private IEnumerator makeNewMagic_go(Vector3 playerPos)
    {
        yield return new WaitForSeconds(0.2f);
        var magic_go = GetNewMagic(CurrentMagic.Name, getMagicShape);
        var startDirection = startFacingOfMagic(ref playerPos);
        magic_go.transform.Translate(playerPos);
        magic_go.GetComponent<MagicCollision>().CurrentMagic = CurrentMagic;

        //Subscribes to the MagicEvent in the magic_go so we can fire and forget, 
        //since when it hits something, it will contain itself in the event, which means we dont have to keep a reference to the magic_go.
        magic_go.GetComponent<MagicCollision>().MagicEvent += DestroyGameObject;

        setInitialDirection(magic_go.GetComponent<Rigidbody2D>(), startDirection);
    }

    /// <summary>
    /// Should make the magic go in the Direction relative to the player.
    /// Meaning if the player is facing forward and the Direction is backwards, the magic should be cast behind the player.
    /// If the player is facing backwards and the Direction is backwards, the magic should still be cast behind the player.
    /// </summary>
    /// <ref name="playerPos">Reference the current position of the player</ref>
    /// <returns>Returns a float with the correct direction relative to the player</returns>
    private float startFacingOfMagic(ref Vector3 playerPos)
    {
        if (CurrentMagic.Direction.Contains("x1"))
        {
            if (player.GetComponent<SpriteRenderer>().flipX)
            {
                playerPos.x += 1;
                return 1;
            }
            playerPos.x -= 1;
            return -1;
        }

        if (CurrentMagic.Direction.Contains("x-1"))
        {
            if(player.GetComponent<SpriteRenderer>().flipX)
            {
                playerPos.x -= 1;
                return -1;
            }
            playerPos.x += 1;
            return 1;
        }
        return 0;
    }


    private int isFlipped()
    {
        if (player.GetComponent<SpriteRenderer>().flipX)
            return 1;
        return -1;
    }

    private void setInitialDirection(Rigidbody2D rb2d, float startDirection)
    {
        if (CurrentMagic.Direction.Contains("y"))
            rb2d.AddForce(new Vector2(0, CurrentMagic.Force * int.Parse(CurrentMagic.Direction.Remove(0,1))));
        else if (CurrentMagic.Direction.Contains("x"))
            rb2d.AddForce(new Vector2(CurrentMagic.Force * startDirection,0));
    }

    /// <summary>
    /// Call this to make the MagicObject
    /// </summary>
    /// <param name="mName">Name of the object</param>
    /// <param name="sprite">The choosen sprite of the magic</param>
    /// <param name="playerPos">Is the current position of the player</param>
    /// <param name="playerFacing">Is the current way the player is facing</param>
    /// <returns>Returns a new GameObject that represents a magic</returns>
    private GameObject GetNewMagic(string mName, Sprite sprite)
    {
        var magic_go = new GameObject(mName);
        var magic_rb2d = magic_go.AddComponent<Rigidbody2D>();
        magic_rb2d.gravityScale = 0;
        var magic_bc2d = magic_go.AddComponent<BoxCollider2D>();
        magic_bc2d.size = new Vector2(0.55f, 0.63f);
        magic_go.AddComponent<MagicCollision>();
        var magic_sr = magic_go.AddComponent<SpriteRenderer>();
        magic_sr.sprite = sprite;
        return magic_go;
    }

    /// <summary>
    /// SPRITE SETUP
    /// </summary>
                #region         SPRITES
    public Sprite AB;
    public Sprite FB;
    public Sprite EB;
    public Sprite WB;
    // Element Balls.
    public Sprite AWH;
    public Sprite FWH;
    public Sprite EWH;
    public Sprite WWH;
    // Element Whirls.
    public Sprite AWA;
    public Sprite FWA;
    public Sprite EWA;
    public Sprite WWA;
    // Element Walls.
    // Element Manipulate_Ball have the same sprite as Element Balls.
    private Sprite getMagicShape
    {
        get
        {
            int tempElementInt = (int)CurrentMagic.Element;
            string typePlusShape = tempElementInt.ToString() + (int)CurrentMagic.Shape;

            switch (int.Parse(typePlusShape))
            {
                case 11:
                    return AB;
                case 21:
                    return FB;
                case 31:
                    return EB;
                case 41:
                    return WB;
                case 12:
                    return AWH;
                case 22:
                    return FWH;
                case 32:
                    return EWH;
                case 42:
                    return WWH;
                case 13:
                    return AWA;
                case 23:
                    return FWA;
                case 33:
                    return EWA;
                case 43:
                    return WWA;
                case 14:
                    return AB;
                case 24:
                    return FB;
                case 34:
                    return EB;
                case 44:
                    return WB;
                default:
                    return new Sprite();
            }
        }
    }
    #endregion

    public MagicBook TheMagicBook
    {
        get { return GameObject.Find("World Keeper").GetComponent<World_Controller>().Player.Averium; ; }
    }

    public Magic CurrentMagic
    {
        get { return TheMagicBook.CurrentMagic; }
    }

    public void DestroyGameObject(object e, MagicHitArgs args)
    {
        if(args.Message.Contains("player"))
            GameObject.Find("World Keeper").GetComponent<World_Controller>().damageSystem.DeductHitPoints(args.HitPoint.Damage);

        Destroy(e as Object);
    }
}
