﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
	
	}

    public GameObject CurrentMagicText;

    public Magic CurrentMagic
    {
        get { return GameObject.Find("Magic System").GetComponent<Magic_System>().CurrentMagic; }
    }
	// Update is called once per frame
	void Update ()
	{
	    CurrentMagicText.GetComponent<Text>().text = CurrentMagic.Name;
	}
}
