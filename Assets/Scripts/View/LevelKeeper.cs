﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
//TODO Currently there's a lot of problems with the fluid. It is harder than I first anticipated

//TODO First. The LevelKeeper has to keep track of all the current tiles in the level.

//TODO Second. It should see if there's a body of water or more in the level, and keep track of them

//TODO Third. It should keep track of the actual tiles holding the water together

//TODO Fourth. It should subscribe to the destruction events for the different tiles.

//TODO Fifth. When one of the tiles is destroyed it should check and see if one of them belongs to the third step.

//TODO Sixth. It should then move the highest and closest tile to the new position

//TODO Seventh.Repeat sixth untill the water is level.


public class LevelKeeper : MonoBehaviour
{
    public GameObject WaterTile;

    private Dictionary<string, Transform> waterTiles;
    private Dictionary<string, Transform> bucketForWater;

    /// <summary>
    /// Contains all the current tiles in the level
    /// </summary>
    private Dictionary<string, Transform> tileList;
    private SortedDictionary<int, List<int>> squareDictionary;
    private float biggestTileY;
    private float biggestTileX;
    private float smallestTileY;
    private float smallestTileX;
    float delayMove;

    // Use this for initialization
    void Start()
    {
        tileList = new Dictionary<string, Transform>();
        waterTiles = new Dictionary<string, Transform>();
        bucketForWater = new Dictionary<string, Transform>();
        delayMove = Time.time + 1f;
        foreach (var tile in GetComponentsInChildren<Transform>())
        {
            if (tile.name.Contains(":("))
            {
                if (tileList.ContainsKey(tile.name.Split(':')[1]))
                    Debug.Log("This tile already exists: " + tile);
                else
                {
                    tileList.Add(tile.name.Split(':')[1], tile);
                }
            }
        }
        waterMove = false;
        setWaterTileList();
        setXYValues();
        findEdgesOfTile(waterTiles);
        findBucketForWater();
    }

    void FixedUpdate()
    {
        //if (waterMove)
        //{
            waterSimStart();
        //foreach (var waterTile_kvp in waterTiles)
        //{
        //    waterSimStart(waterTile_kvp.Value);
        //}
    //}
    }

    private bool waterMove;
    #region Water moving
    private void waterSimStart()
    {
        if (delayMove <= Time.time && waterMove)
        {
            foreach (var waterTile_kvp in waterTiles)
            {
                circleCastTile(waterTile_kvp.Value);
                delayMove = Time.time + 1f;                
            }           
        }
    }

    // DO NOT CHANGE THIS VALUE. It have been calculated beforehand. It is a^2 + b^2 = c^2 => sqr(c^2) = radius of the needed circle to do the CircleCastAll
    private float radiusOfCircleCast = 0.7071006f;
    private void circleCastTile(Transform waterTile_tr)
    {
        
        var raycastArray = Physics2D.CircleCastAll(waterTile_tr.transform.position, radiusOfCircleCast, Vector2.zero);
        var sum = raycastArray.Where(ray => ray.transform.tag.Contains("Magic")).Aggregate(raycastArray.Length, (current, ray) => current - 1);

        Vector3 returnedPos = waterTile_tr.position;
        if (lookAbove(raycastArray, waterTile_tr, ref returnedPos) && sum != 5)
        {
            waterTile_tr.position = returnedPos;
            waterMove = true;          
        }      
    }

    private bool lookAbove(IEnumerable<RaycastHit2D> raycastArray, Transform waterTile_tr, ref Vector3 returnedPos)
    {
        bool moveOrNot = true;
        bool leftTile = false;
        bool rightTile = false;
        bool bottomTile = false;
        foreach (var ray in raycastArray)
        {
            if (ray.transform.name.Contains("Magic"))
                break;
            if (Mathf.FloorToInt(ray.transform.position.x - 1) == Mathf.FloorToInt(waterTile_tr.position.x))
                rightTile = true;
            if (Mathf.FloorToInt(ray.transform.position.x + 1) == Mathf.FloorToInt(waterTile_tr.position.x))
                leftTile = true;
            if (Mathf.FloorToInt(ray.transform.position.y + 1) == Mathf.FloorToInt(waterTile_tr.position.y))
                bottomTile = true;
        }

        if (rightTile && leftTile && bottomTile)
        {
            moveOrNot = false;
        }
        if (!bottomTile)
        {
            returnedPos.y -= 1;
            return true;
        }
        if (!rightTile)
        {
            returnedPos.x += 1;
        }
        if (!leftTile)
        {
            returnedPos.x -= 1;
        }

        return moveOrNot;
    }
    #endregion
    /// <summary>
    /// Call this from a water tile to check if it is destroyable.
    /// </summary>
    /// <param name="waterTileTransform">Is the transform from the water tile</param>
    /// <returns>a bool based on if it can be destroyed. True = Destroyable</returns>
    public bool CanWaterTileBeDestroyed(Transform waterTileTransform)
    {
        if (!isWaterRectangle())
            return false;

        var vector3 = waterTileTransform.position;
        if (Mathf.FloorToInt(vector3.y) != Mathf.FloorToInt(biggestTileY))
            return false;

        var vector3AsString = vector3.ToString();
        foreach (var tile in waterTiles)
        {
            if (tile.Key.Contains(vector3AsString))
            {
                return true;
            }
        }
        return false;
    }

    private void findEdgesOfTile(Dictionary<string, Transform> tileDictionary)
    {
        biggestTileY = tileDictionary.FirstOrDefault().Value.position.y;
        biggestTileX = tileDictionary.FirstOrDefault().Value.position.x;
        smallestTileY = tileDictionary.FirstOrDefault().Value.position.y;
        smallestTileX = tileDictionary.FirstOrDefault().Value.position.x;
        foreach (var tile in tileDictionary)
        {
            //Should find the biggest value for y
            biggestTileY = findBiggestValue(tile.Value.transform.position.y, biggestTileY);
            //Should find the biggest value for x
            biggestTileX = findBiggestValue(tile.Value.transform.position.x, biggestTileX);
            //Should find the smallest value for y
            smallestTileY = findSmallestValue(tile.Value.transform.position.y, smallestTileY);
            //Should find the smallest value for x
            smallestTileX = findSmallestValue(tile.Value.transform.position.x, smallestTileX);
        }
    }

    /// <summary>
    /// Call this to check if a body of water is a rectangle. 
    /// </summary>
    /// <returns>a bool based on if it a rectangle. True = Rectangle</returns>
    private bool isWaterRectangle()
    {
        bool isItRectangle = false;
        // What happens here is that we need to compare 2 different X's here and their Y values.
        foreach (var kvpOri in squareDictionary)
        {

            var tempKvp = kvpOri;
            foreach (var kvp in squareDictionary)
            {
                if (tempKvp.Key != kvp.Key)
                {
                    //Here the X values are checked for if they are a cont. of each other
                    if (((tempKvp.Key - kvp.Key) == 1) || ((kvp.Key - tempKvp.Key) == 1))
                    {
                        tempKvp.Value.Sort();
                        kvp.Value.Sort();
                        //If this is true we return immediately because that means there is one or more y in the x's list and that can't be a rectangle
                        if (tempKvp.Value.Count != kvp.Value.Count)
                        {
                            Debug.Log("tempKvp value count against kvp value count was false");
                            return false;
                        }
                        if (tempKvp.Value.Count == kvp.Value.Count)
                        {
                            Debug.Log("tempKvp value count against kvp value count was true");
                            for (int i = 0; i < kvp.Value.Count; i++)
                            {
                                // Here is where the real magic happens. We substract x1 : y1 with x2 : y1 and so on. Every one of these NEEDS to be zero because that means they are equal.
                                Debug.Log("Checking to see if the values in the kvp list are the same");
                                if ((tempKvp.Value[i] - kvp.Value[i]) == 0)
                                {
                                    var tempNr = tempKvp.Value[i] - kvp.Value[i];
                                    Debug.Log("This should be 0. Is it?: " + tempNr);
                                    isItRectangle = true;
                                }
                            }

                        }
                    }
                }
            }
        }

        return isItRectangle;
    }

    /// <summary>
    /// Collects and sort through all the water tiles, pairing it up in x1 : y1,y2,y3,y4. x2 : y1,y2,y3,y4
    /// When you want to check and see if it's sqaure, "all" you have to is is basically y1-y1, y2-y2 and so on.
    /// </summary>
    private void setXYValues()
    {
        squareDictionary = new SortedDictionary<int, List<int>>();
        foreach (var tile_kvp in waterTiles)
        {
            var x = Mathf.FloorToInt(tile_kvp.Value.position.x);
            var y = Mathf.FloorToInt(tile_kvp.Value.position.y);

            foreach (var kvp in squareDictionary)
            {
                if (kvp.Key == x)
                    kvp.Value.Add(y);
            }
            if (!squareDictionary.ContainsKey(x))
            {
                squareDictionary.Add(x, new List<int> { y });
            }
        }

    }

    private void setWaterTileList()
    {
        foreach (var tile_kvp in tileList)
        {
            if (tile_kvp.Value.name.Contains("Water"))
            {
                waterTiles.Add(tile_kvp.Key, tile_kvp.Value);
                setDestructionEvent(tile_kvp.Value);
            }
        }
    }

    /// <summary>
    /// Finds the tiles around the body of water.
    /// </summary>
    private void findBucketForWater()
    {
        foreach (var tile in waterTiles)
        {
            if (tile.Key.Contains("(" + biggestTileX))
            {
                var tempSplit = tile.Key.Replace("(", "").Replace(")", "").Split(',');
                var tileX = float.Parse(tempSplit[0]);
                var tileY = float.Parse(tempSplit[1]);
                var tileZ = float.Parse(tempSplit[2]);
                tileX += 1;
                addBucketTile(tileX, tileY, tileZ);
            }
            if (tile.Key.Contains("(" + smallestTileX))
            {
                var tempSplit = tile.Key.Replace("(", "").Replace(")", "").Split(',');
                var tileX = float.Parse(tempSplit[0]);
                var tileY = float.Parse(tempSplit[1]);
                var tileZ = float.Parse(tempSplit[2]);
                tileX -= 1;
                addBucketTile(tileX, tileY, tileZ);
            }
            if (tile.Key.Contains(smallestTileY + ".0, 0.0)"))
            {
                var tempSplit = tile.Key.Replace("(", "").Replace(")", "").Split(',');
                var tileX = float.Parse(tempSplit[0]);
                var tileY = float.Parse(tempSplit[1]);
                var tileZ = float.Parse(tempSplit[2]);
                tileY -= 1;
                addBucketTile(tileX, tileY, tileZ);
            }
        }
    }

    private void addBucketTile(float tileX, float tileY, float tileZ)
    {
        var tempKey = "(" + tileX + ".0, " + tileY + ".0, " + tileZ + ".0)";
        if (tileList.ContainsKey(tempKey))
        {
            var tempTile = tileList[tempKey];
            addItemToDictionary(bucketForWater, tempTile);
            setDestructionForBucketEvent(tempTile);
        }
    }

    private bool addItemToDictionary(Dictionary<string, Transform> dictionary, Transform gameObjectToBeAdded)
    {
        if (!dictionary.ContainsKey(gameObjectToBeAdded.position.ToString()))
        {
            dictionary.Add(gameObjectToBeAdded.position.ToString(), gameObjectToBeAdded);
            return true;
        }
        return false;
    }

    #region Destruction methods
    private void setDestructionEvent(Transform tile)
    {
        if (tile.GetComponent<OnDestructionChanged>() == null)
            return;
        tile.GetComponent<OnDestructionChanged>().DestructionEvent += onDestruction;
    }

    /// <summary>
    /// This is called when a water tile is destroyed. That way the level keeper knows when to run the water simulation.
    /// </summary>
    /// <param name="e">The object that was destroyed</param>
    /// <param name="args">The TileDestructionArgs</param>
    private void onDestruction(object e, TileDestructionArgs args)
    {
        var tile = RemoveTileFromDictionaries(e);

    }

    private void setDestructionForBucketEvent(Transform tempTile)
    {
        if (tempTile.GetComponent<OnDestructionChanged>() == null)
            return;
        tempTile.GetComponent<OnDestructionChanged>().DestructionEvent += onDestructionBucket;
    }

    private void onDestructionBucket(object sender, TileDestructionArgs e)
    {
        var tile = (GameObject)sender;
        bucketForWater.Remove(tile.GetComponent<Transform>().name.Split(':')[0]);
        waterMove = true;
        Debug.Log("Water should move");
    }
    private GameObject RemoveTileFromDictionaries(object e)
    {
        var tile = e as GameObject;
        var key = tile.name.Split(':')[1];
        waterTiles.Remove(key);
        tileList.Remove(key);
        setXYValues();
        return tile;
    }
    #endregion

    private static float findSmallestValue(float value, float smallestTileY)
    {
        if (value <= smallestTileY)
            smallestTileY = value;
        return smallestTileY;
    }

    private static float findBiggestValue(float value, float biggestTileY)
    {
        if (value >= biggestTileY)
            biggestTileY = value;
        return biggestTileY;
    }
}

