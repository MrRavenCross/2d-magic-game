﻿using UnityEngine;
using System.Collections;

public class EarthProperty : MyMonoBehaviorPlus
{
    public Sprite grassMidCenter;
    public Sprite grassMidHalf;
    public Sprite grassMid;
    private Sprite[] nameCol;
    public int SpriteNr = 4;
    // Use this for initialization
    void Start ()
    {
	    nameCol = new []{grassMid, grassMidCenter, grassMidHalf };
        if (SpriteNr == 4)
        {
            var som = Physics2D.RaycastAll(transform.position, Vector2.up);
            var sum = transform.position - som[1].transform.position;
            if (sum.y <= 1.1f)
            {
                GetComponent<SpriteRenderer>().sprite = nameCol[1];
            }
        }
        else
            GetComponent<SpriteRenderer>().sprite = nameCol[SpriteNr];
    }

    // Update is called once per frame
    void Update ()
    {
	
	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        var magic = coll.transform.GetComponent<MagicCollision>();
        if (magic != null)
        {
            if (magic.CurrentMagic.Element == MagicElement.Earth && SpriteNr <= 3 && SpriteNr != 0)
            {
                SpriteNr--;
                GetComponent<SpriteRenderer>().sprite = nameCol[SpriteNr];
                var rb2d = GetComponent<BoxCollider2D>();
                rb2d.offset = new Vector2(0, 0);
                rb2d.size = new Vector2(1, 1);
                return;
            }

            if (magic.CurrentMagic.Element == MagicElement.Water)
            {
                if (SpriteNr >= 2 || magic.CurrentMagic.Direction == magic.CurrentMagic.SecondDirection)
                {
                    Destroy(GetComponent<Transform>().gameObject);
                    GetComponent<OnDestructionChanged>().onDestructionChanged(gameObject,new TileDestructionArgs("Grass/Dirt was destroyed"));
                    return;
                }

                if (SpriteNr < 3)
                {                    
                    SpriteNr++;
                    if (SpriteNr == 2)
                    {
                        var rb2d = GetComponent<BoxCollider2D>();
                        rb2d.offset = new Vector2(0, -0.2154398f);
                        rb2d.size = new Vector2(1, 0.5691197f);
                    }
                    GetComponent<SpriteRenderer>().sprite = nameCol[SpriteNr];                    
                }                
            }
        }
    }
}
