﻿using UnityEngine;
using System.Collections;

public class BedrockProperty : MyMonoBehaviorPlus
{
    public Sprite stoneMidCenter;
    public Sprite stoneMid;
    private bool HasBeenHit = false;
    private Sprite[] nameCol;
    // Use this for initialization
    void Start()
    {
        nameCol = new[] { stoneMid, stoneMidCenter};
        SetName();
        var som = Physics2D.RaycastAll(transform.position, Vector2.up);
        if (som.Length != 1)
        {
            var sum = transform.position - som[1].transform.position;
            if (sum.y <= 1.1f)
            {
                GetComponent<SpriteRenderer>().sprite = nameCol[1];
            }
        }
    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        //Debug.Log(name);
        //Debug.Log(coll.transform.name);
        var checkIfMagic = coll.transform.GetComponent<MagicCollision>();
        if (checkIfMagic == null)
            return;

        if (HasBeenHit)
        {
            Destroy(this);
        }
        else
            GetComponent<SpriteRenderer>().sprite = stoneMidCenter;

    }
}
