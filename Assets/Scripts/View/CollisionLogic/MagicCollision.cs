﻿using System;
using UnityEngine;
using System.Collections;
using System.ComponentModel;
using System.Net.Sockets;
using UnityEditor;
using UnityEngine.Events;
/// <summary>
/// REMEMBER TO SET THE CurrentMagic
/// </summary>
public class MagicCollision : MonoBehaviour
{
    public event EventHandler<MagicHitArgs> MagicEvent;

    public Magic CurrentMagic;
    private bool canManipulate;
    private float lifeTime;
    void Start()
    {
        tag = "Magic";
        if (CurrentMagic.Shape == MagicShape.Manipulate_Ball)
        {
            canManipulate = true;
            lifeTime = Time.time + 5;
        }
    }

    void FixedUpdate()
    {
        if (canManipulate)
        {
            if (Input.GetMouseButton(1))
            {
                var curPos = GetComponent<Transform>().position;
                var furPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                var direction = new Vector2(furPos.x - curPos.x, furPos.y - curPos.y);
                direction.Normalize();
                Debug.Log("Direction: " + direction);
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                GetComponent<Rigidbody2D>().AddForce(direction * (CurrentMagic.Force));

            }

            if (Time.time >= lifeTime)
                OnMagicHit(new MagicHitArgs("Manipulate_ball expired"));
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.transform.name == "Player")
        {
            OnMagicHit(new MagicHitArgs("Magic hit the player", new HitPointArgs(CurrentMagic.Name, CurrentMagic.Damage)));
            return;
        }
        if(CurrentMagic.Direction == CurrentMagic.SecondDirection)
            OnMagicHit(new MagicHitArgs("Magic hit Something"));

        if (CurrentMagic.HasHit)
        {            
            OnMagicHit(new MagicHitArgs("Magic hit Something"));
        }
        else
        {
            var rb2d = GetComponent<Rigidbody2D>();
            if (CurrentMagic.SecondDirection.Contains("y"))
            {
                rb2d.velocity = Vector2.zero;
                rb2d.AddForce(new Vector2(0, CurrentMagic.Force*int.Parse(CurrentMagic.SecondDirection.Replace("y", ""))));
            }
            else if (CurrentMagic.SecondDirection.Contains("x"))
            {
                rb2d.velocity = Vector2.zero;
                rb2d.AddForce(new Vector2(CurrentMagic.Force*int.Parse(CurrentMagic.SecondDirection.Replace("x", "")), 0));
            }
        }      
       
    }

    void OnTriggerEnter2D()
    {
        OnMagicHit(new MagicHitArgs("It hit some water"));
    }

    private void OnMagicHit(MagicHitArgs e)
    {
        var tempMagicEvent = MagicEvent;
        if (tempMagicEvent != null)
        {
            tempMagicEvent(gameObject, e);
        }
    }
    
}

