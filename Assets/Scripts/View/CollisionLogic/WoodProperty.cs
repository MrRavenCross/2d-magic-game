﻿using UnityEngine;
using System.Collections;

public class WoodProperty : MyMonoBehaviorPlus {

    public Sprite woodMidCenter;
    public Sprite woodMidHalf;
    public Sprite woodMid;
    private Sprite[] nameCol;
    private int nr;
    // Use this for initialization
    void Start()
    {
        nameCol = new[] { woodMid, woodMidCenter, woodMidHalf };
        SetName();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        var magic = coll.transform.GetComponent<MagicCollision>();
        if (magic != null)
        {            
            if (magic.CurrentMagic.Element == MagicElement.Fire)
            {
                if (nr >= 2 || magic.CurrentMagic.Direction == magic.CurrentMagic.SecondDirection)
                {
                    Destroy(GetComponent<Transform>().gameObject);
                    GetComponent<OnDestructionChanged>().onDestructionChanged(gameObject, new TileDestructionArgs("Wood was destroyed"));
                    return;
                }

                if (nr < 3)
                {
                    nr++;
                    if (nr == 2)
                    {
                        var rb2d = GetComponent<BoxCollider2D>();
                        rb2d.offset = new Vector2(0, -0.2154398f);
                        rb2d.size = new Vector2(1, 0.5691197f);
                    }
                    GetComponent<SpriteRenderer>().sprite = nameCol[nr];
                }
            }
        }
    }
}
