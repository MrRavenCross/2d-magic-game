﻿using UnityEngine;
using System.Collections;

public class FireProperty : MonoBehaviour {

    public Sprite fireMidCenter;
    public Sprite fireMidHalf;
    public Sprite fireMid;
    private Sprite[] nameCol;
    private int nr;
    // Use this for initialization
    void Start()
    {
        nameCol = new[] { fireMid, fireMidCenter, fireMidHalf };
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        var magic = coll.transform.GetComponent<MagicCollision>();
        if (magic != null)
        {
            if ((magic.CurrentMagic.Element == MagicElement.Fire || magic.CurrentMagic.Element == MagicElement.Air) && nr <= 3 && nr != 0)
            {
                nr--;
                GetComponent<SpriteRenderer>().sprite = nameCol[nr];
                var rb2d = GetComponent<BoxCollider2D>();
                rb2d.offset = new Vector2(0, 0);
                rb2d.size = new Vector2(1, 1);
                return;
            }

            if (magic.CurrentMagic.Element == MagicElement.Water)
            {
                if (nr >= 2 || magic.CurrentMagic.Direction == magic.CurrentMagic.SecondDirection)
                {
                    Destroy(GetComponent<Transform>().gameObject);
                    return;
                }

                if (nr < 3)
                {
                    nr++;
                    if (nr == 2)
                    {
                        var rb2d = GetComponent<BoxCollider2D>();
                        rb2d.offset = new Vector2(0, -0.2154398f);
                        rb2d.size = new Vector2(1, 0.5691197f);
                    }
                    GetComponent<SpriteRenderer>().sprite = nameCol[nr];
                }
            }
        }
    }
}
