﻿using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using System.Runtime.CompilerServices;

public class WaterProperty : MyMonoBehaviorPlus
{

    // Use this for initialization
    public Sprite Water;
    public Sprite WaterHalf;
    private Sprite[] nameCol;
    public GameObject DirtPrefab;
    public int SpriteStartNr;
    // Use this for initialization
    void Start()
    {
        nameCol = new[] { Water, WaterHalf };
        SetName();
        tileStage = nameCol[SpriteStartNr].name;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        var magic = coll.transform.GetComponent<MagicCollision>();
        if (magic != null)
        {
            if (lastWaterTile())
            {
                if (magic.CurrentMagic.Element == MagicElement.Water && SpriteStartNr <= 1 && SpriteStartNr != 0)
                {
                    SpriteStartNr--;
                    GetComponent<SpriteRenderer>().sprite = nameCol[SpriteStartNr];
                    var rb2d = GetComponent<BoxCollider2D>();
                    rb2d.offset = new Vector2(0, 0);
                    rb2d.size = new Vector2(1, 1);
                    return;
                }
                if (magic.CurrentMagic.Element == MagicElement.Air)
                {
                    if (magic.CurrentMagic.Direction == magic.CurrentMagic.SecondDirection)
                    {
                        //Make some fucking ice out of this water please. //TODO: MAKE ICE
                    }
                }
                if (magic.CurrentMagic.Element == MagicElement.Earth || magic.CurrentMagic.Element == MagicElement.Fire)
                {
                    if (SpriteStartNr >= 1 ||
                        magic.CurrentMagic.Direction == magic.CurrentMagic.SecondDirection &&
                        magic.CurrentMagic.Element != MagicElement.Fire)
                    {

                        Destroy(GetComponent<Transform>().gameObject);
                        GetComponent<OnDestructionChanged>().onDestructionChanged(gameObject, new TileDestructionArgs("Tile at: " + gameObject.transform.position + " was destroyed"));
                        return;
                    }

                    if (SpriteStartNr < 2)
                    {
                        SpriteStartNr++;
                        if (SpriteStartNr == 1)
                        {
                            var rb2d = GetComponent<BoxCollider2D>();
                            rb2d.offset = new Vector2(0, -0.2154398f);
                            rb2d.size = new Vector2(1, 0.5691197f);
                        }
                        GetComponent<SpriteRenderer>().sprite = nameCol[SpriteStartNr];
                    }
                }
            }
        }
    }


    private string tileStage;
    

    /// <summary>
    /// This method will try and find the 4 nearby water tiles. If they are not found, it will give the OK to change sprite or destroy.
    /// REMEMBER TO NAME THE FRICKING WATERTILES CORRECTLY:::: Water Prefab. No (1) or (2) and so on. Just Water Prefab or WaterHalf Prefab.
    /// </summary>
    /// <returns>Will give a true if there is no other tile above it</returns>
    private bool lastWaterTile()
    {
        return GameObject.Find("Level").GetComponent<LevelKeeper>().CanWaterTileBeDestroyed(transform);
    }
}
