﻿using System;
using UnityEngine;
using System.Collections;

public class AirProperty : MyMonoBehaviorPlus {

    // Use this for initialization
    public Sprite airMidCenter;
    public Sprite airMidHalf;
    public Sprite airMid;
    private Sprite[] nameCol;
    private int nr;
    // Use this for initialization
    void Start()
    {
        nameCol = new[] { airMid, airMidCenter, airMidHalf };
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        var magic = coll.transform.GetComponent<MagicCollision>();
        if (magic != null)
        {
            if (magic.CurrentMagic.Element == MagicElement.Air && nr <= 3 && nr != 0)
            {
                nr--;
                GetComponent<SpriteRenderer>().sprite = nameCol[nr];
                var rb2d = GetComponent<BoxCollider2D>();
                rb2d.offset = new Vector2(0, 0);
                rb2d.size = new Vector2(1, 1);
                return;
            }

            if (magic.CurrentMagic.Element == MagicElement.Fire)
            {
                if (nr >= 2 || magic.CurrentMagic.Direction == magic.CurrentMagic.SecondDirection)
                {
                    Destroy(GetComponent<Transform>().gameObject);
                    GetComponent<OnDestructionChanged>().onDestructionChanged(gameObject,new TileDestructionArgs("Tile at: "+ gameObject.transform.position + " was destroyed"));
                    return;
                }

                if (nr < 3)
                {
                    nr++;
                    if (nr == 2)
                    {
                        var rb2d = GetComponent<BoxCollider2D>();
                        rb2d.offset = new Vector2(0, -0.2154398f);
                        rb2d.size = new Vector2(1, 0.5691197f);
                    }
                    GetComponent<SpriteRenderer>().sprite = nameCol[nr];
                }
            }
        }
    }
}
