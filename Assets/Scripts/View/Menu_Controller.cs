﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using UnityEditor;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Menu_Controller : MonoBehaviour
{
    private GameObject magicBookPage1;
    private GameObject magicBookPage2;
    private GameObject makingMagicText_go;
    private bool isPopUpOpen;
    private float delay;
    //A prefab that is used when the magic page is filled out.
    public GameObject Button_StandardMagic;
    //Holds the map for which button goes to which magic in the MagicBook.
    private Dictionary<string, GameObject> buttonToMagic;

    // Use this for initialization
    void Start()
    {
        buttonToMagic = new Dictionary<string, GameObject>();
        magicBookPage1 = GameObject.Find("MagicBookPage1");
        magicBookPage2 = GameObject.Find("MagicBookPage2");
        makingMagicText_go = GameObject.Find("Textbox_MagicName");
        magicBookPage1.SetActive(false);
        magicBookPage2.SetActive(false);
        delay = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (magicBookPage1 == null)
            magicBookPage1 = GameObject.Find("MagicBookPage1");
        if (magicBookPage2 == null)
            magicBookPage2 = GameObject.Find("MagicBookPage2");
        if (Input.GetKeyUp(KeyCode.P))
        {
            OnClickOpenClose();
        }
    }

    public void SetPage2()
    {
        // Int a is to make sure that there can't be added more magics than there are in the MagicBook.
        int a = 0;
        //This is for setting up the magic page 1 and 2 correctly.
        for (int i = 1; i <= 2; i++)
        {
            //There can be 13 different magics per page.
            for (int j = 1; j <= 13; j++)
            {
                if (!buttonToMagic.ContainsKey(TheMagicBook.Pages.ElementAt(a).Key))
                {

                    var button = Instantiate(Button_StandardMagic);
                    button.transform.SetParent(GameObject.Find("MagicList_" + i).transform);
                    button.transform.localScale = new Vector3(1, 1, 1);
                    button.name = TheMagicBook.Pages.ElementAt(a).Key;
                    button.GetComponentInChildren<Text>().text = TheMagicBook.Pages.ElementAt(a).Key;
                    var page1 = TheMagicBook.Pages.ElementAt(a);
                    button.GetComponent<Button>().onClick.AddListener(() => { OnButtonMagicClick(page1.Key); });
                    buttonToMagic.Add(TheMagicBook.Pages.ElementAt(a).Key, button);
                }
                a++;
                if (a >= TheMagicBook.Count)
                    break;
            }
            if (a >= TheMagicBook.Count)
                break;
        }
    }
    /// <summary>
    /// Shows the current name of the magic the player is making in the MagicBook
    /// </summary>
    private void currentMakingMagicName()
    {
        var currentName = Element.ToString() + Shape + " " + Magic.FindDirectionName(Direction, SecondDirection);        
        makingMagicText_go.GetComponent<Text>().text = currentName.Replace("Empty", "");
    }

    #region Properties
    public MagicElement Element { get; private set; }
    public MagicShape Shape { get; private set; }
    public string Direction { get; private set; }
    public string SecondDirection { get; private set; }
    private Magic newMagic { get; set; }
    public Magic CurrentMagic
    {
        get { return GameObject.Find("World Keeper").GetComponent<World_Controller>().Player.Averium.CurrentMagic; }
    }
    public MagicBook TheMagicBook
    {
        get { return GameObject.Find("World Keeper").GetComponent<World_Controller>().Player.Averium; }
    }
    #endregion

    public void OnClickOpenClose()
    {
        if (!isPopUpOpen && Time.time >= delay)
        {
            delay = Time.time + 0.2f;
            magicBookPage1.SetActive(true);
            isPopUpOpen = true;
        }
        else if (isPopUpOpen && Time.time >= delay)
        {
            delay = Time.time + 0.2f;
            magicBookPage1.SetActive(false);
            magicBookPage2.SetActive(false);
            isPopUpOpen = false;
        }
    }

    public void OnClickMakeMagic()
    {
        if (Shape == MagicShape.Empty)
            return;
        if (Element == MagicElement.Empty)
            return;
        if (Shape == MagicShape.Ball)
        {
            if (string.IsNullOrEmpty(Direction))
                return;
        }
        resetTextbox_Error();
        newMagic = new Magic(Element, Shape, Direction, SecondDirection);
        GameObject.Find("World Keeper").GetComponent<World_Controller>().Player.Averium.AddPage(newMagic);

    }

    public void OnClickElementPick(int element)
    {
        if ((int) Element == element)
        {
            Element = MagicElement.Empty;
            currentMakingMagicName();
            return;
        }
        switch (element)
        {
            case 1:
                Element = MagicElement.Air;
                break;
            case 2:
                Element = MagicElement.Fire;
                break;
            case 3:
                Element = MagicElement.Earth;
                break;
            case 4:
                Element = MagicElement.Water;
                break;
        }
        currentMakingMagicName();
    }

    public void OnClickDirection(string direction)
    {
        if (direction.Contains("SD"))
        {
            var secDir = direction.Split(':')[1];
            if (secDir == SecondDirection)
                SecondDirection = "";
            else
                SecondDirection = secDir;
        }
        else
        {
            var dir = direction.Split(':')[1];
            if (dir == Direction)
                Direction = "";
            else
                Direction = dir;
        }
        currentMakingMagicName();
    }

    public void OnClickShapePick(int shape)
    {
        if ((int)Shape == shape)
        {
            Shape = MagicShape.Empty;;
            currentMakingMagicName();
            return;
        }
        switch (shape)
        {
            case 1:
                Shape = MagicShape.Ball;
                if (string.IsNullOrEmpty(Direction))
                    TextboxError.text = "The direction is missing";
                setOptions(true, true);
                break;
            case 2:
                Shape = MagicShape.Whirl;
                setOptions(true, true);
                break;
            case 3:
                Shape = MagicShape.Wall;
                setOptions(false, false);
                SecondDirection = "";
                break;
            case 4:
                Shape = MagicShape.Manipulate_Ball;
                Direction = "x1";
                SecondDirection = "";
                setOptions(false, true);
                break;
        }
        //if (Shape != MagicShape.Manipulate_Ball)
        //    setOptions(true,true);

        currentMakingMagicName();
    }

    private GameObject direction_go;
    private GameObject secondDirection_go;
    private void setOptions(bool onOff, bool both)
    {
        if (direction_go == null)
            direction_go = GameObject.Find("Direction");
        if (secondDirection_go == null)
            secondDirection_go = GameObject.Find("SecondDirection");

        if (both)
        {
            direction_go.SetActive(onOff);
            secondDirection_go.SetActive(onOff);
        }
        else
            secondDirection_go.SetActive(onOff);
    }
    //Max 13 magics per page.
    public void OnClickNextPage(int page)
    {
        if (page == 2)
        {
            magicBookPage1.SetActive(false);
            magicBookPage2.SetActive(true);
            SetPage2();
        }
        else if (page == 1)
        {
            magicBookPage1.SetActive(true);
            magicBookPage2.SetActive(false);
        }
    }

    public void OnButtonMagicClick(string magicName)
    {
        if (buttonToMagic.ContainsKey(magicName))
        {
            TheMagicBook.CurrentMagic = TheMagicBook.Pages[magicName];
        }
    }

    private Text textbox_Error;
    private Text TextboxError
    {
        get
        {
            if (textbox_Error == null)
                textbox_Error = GameObject.Find("Textbox_Error").GetComponent<Text>();
            return textbox_Error;
        }
    }

    private void resetTextbox_Error()
    {
        TextboxError.text = "";
    }
}



