﻿using System;
using UnityEngine;
using System.Collections;

public class MagicHitArgs : EventArgs
{
    public MagicHitArgs(string s, HitPointArgs hitPointArgs = null)
    {
        message = s;
        HitPoint = hitPointArgs;
    }
    private string message;

    public string Message
    {
        get { return message; }
        set { message = value; }
    }

    private HitPointArgs hitPoint;

    public HitPointArgs HitPoint
    {
        get { return hitPoint; }
        set { hitPoint = value; }
    }
}
