﻿using System;
using UnityEngine;
using System.Collections;

public class MagicChangedArgs : EventArgs
{
    public MagicChangedArgs(string s)
    {
        message = s;
    }
    private string message;

    public string Message
    {
        get { return message; }
        set { message = value; }
    }
}

