﻿using System;
using UnityEngine;
using System.Collections;

public class TileDestructionArgs : EventArgs {

    // Use this for initialization
    public TileDestructionArgs(string s)
    {
        message = s;
    }
    private string message;

    public string Message
    {
        get { return message; }
        set { message = value; }
    }
}
