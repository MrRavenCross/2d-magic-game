﻿using System;
using UnityEngine;
using System.Collections;

public class TileMovedArgs : EventArgs {

    public TileMovedArgs(Vector3 oldPosition, Vector3 newPosition)
    {
        OldPosition = oldPosition;
        NewPosition = newPosition;
    }
    private Vector3 oldPosition;

    public Vector3 OldPosition
    {
        get { return oldPosition; }
        set { oldPosition = value; }
    }

    private Vector3 newPostition;
    public Vector3 NewPosition
    {
        get { return newPostition; }
        set { newPostition = value; }
    }
}
