﻿using System;
using UnityEngine;
using System.Collections;

public class GameOverArgs : EventArgs
{

    public GameOverArgs(string reason)
    {
        message = reason;
    }
    private string message;

    public string Message
    {
        get { return message; }
        set { message = value; }
    }
}
