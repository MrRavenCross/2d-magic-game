﻿using System;
using UnityEngine;
using System.Collections;

public class HitPointArgs : EventArgs
{
    private string message;
    private int damage;

    public HitPointArgs(string s, int damage)
    {
        message = s;
        Damage = damage;
    }
    
    public string Message
    {
        get { return message; }
        set { message = value; }
    }

    public int Damage
    {
        get { return damage; }
        set { damage = value; }
    }
}
