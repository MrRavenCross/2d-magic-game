﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using UnityEditor;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Model
{
    public class DamageSystem
    {
        private Player player;
        public event EventHandler<GameOverArgs> GameOverEventHandler;
        public DamageSystem(Player player)
        {
            this.player = player;            
        }

        public void DeductHitPoints(int damage)
        {
            player.HitPoints -= damage;
            if(player.HitPoints <=0)
                OnGameOver(player, new GameOverArgs("Health ran out"));
        }

        public void AddHitPoints(int points)
        {
            player.HitPoints += points;
        }
        private void OnGameOver(object obj, GameOverArgs e)
        {
            var tempMagicEvent = GameOverEventHandler;
            if (tempMagicEvent != null)
            {
                tempMagicEvent(obj, e);
            }
        }
    }
}
