﻿using System;
using UnityEngine;
using System.Collections;
/// <summary>
/// Modify this class if there's functions that needs to be there for all classes that inherit MonoBehavior
/// </summary>
public class MyMonoBehaviorPlus : MonoBehaviour
{
    private Vector3 posVector3;
    public void SetName()
    {
        posVector3 = GetComponent<Transform>().position;
        name = name + ":" + posVector3;
    }
    
}
