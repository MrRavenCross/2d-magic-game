﻿using System;
using UnityEngine;
using System.Collections;

public class OnDestructionChanged : MonoBehaviour
{
    public event EventHandler<TileDestructionArgs> DestructionEvent;

    public void onDestructionChanged(object obj, TileDestructionArgs e)
    {
        var tempMagicEvent = DestructionEvent;
        if (tempMagicEvent != null)
        {
            tempMagicEvent(obj, e);
        }

    }
}
