﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading;
public class Fluid : MonoBehaviour
{
    float delayMove;
	// Use this for initialization
	void Start ()
	{
	    delayMove = Time.time + 1f;
	}
	void Update()
    {
        
    }
	// Update is called once per frame
	void FixedUpdate ()
    {
        var raycastArray = Physics2D.CircleCastAll(GetComponent<Transform>().transform.position, radiusOfCircleCast, Vector2.zero);
        var sum = raycastArray.Where(ray => ray.transform.tag.Contains("Magic")).Aggregate(raycastArray.Length, (current, ray) => current - 1);
        var spriteStage = GetComponent<WaterProperty>().SpriteStartNr;
        if (sum != 5 && delayMove <= Time.time)
        {
            var thread2 = new Thread(circleCastTile);
            thread2.Start();
            while(thread2.IsAlive)
            {

            }
            delayMove = Time.time + 1f;
        }
    }
    public bool RecalculateSum { get; set; }

    // DO NOT CHANGE THIS VALUE. It have been calculated beforehand. It is a^2 + b^2 = c^2 => sqr(c^2) = radius of the needed circle to do the CircleCastAll
    private float radiusOfCircleCast = 0.7071006f;
    private void circleCastTile()
    {
        var raycastArray = Physics2D.CircleCastAll(GetComponent<Transform>().transform.position, radiusOfCircleCast, Vector2.zero);
        var sum = raycastArray.Where(ray => ray.transform.tag.Contains("Magic")).Aggregate(raycastArray.Length, (current, ray) => current - 1);

        //Debug.Log(sum.ToString());
        Vector3 returnedPos = GetComponent<Transform>().position;
        if (lookAbove(raycastArray, ref returnedPos) && sum != 5)
        {
            transform.position = returnedPos;
            //Debug.Log("Water should have moved now");
        }
        if(sum == 5)
            RecalculateSum = false;
    }

    private bool lookAbove(IEnumerable<RaycastHit2D> raycastArray, ref Vector3 returnedPos)
    {
        bool leftTile = false;
        bool rightTile = false;
        bool bottomTile = false;
        foreach (var ray in raycastArray)
        {
            //Debug.Log(ray.transform.name);
            //Check both sides for a tile. If found then false
            if (Mathf.FloorToInt(ray.transform.position.x - 1) == Mathf.FloorToInt(transform.position.x))
                rightTile = true;
            if (Mathf.FloorToInt(ray.transform.position.x + 1) == Mathf.FloorToInt(transform.position.x))
                leftTile = true;
            if (Mathf.FloorToInt(ray.transform.position.y + 1) == Mathf.FloorToInt(transform.position.y))
                bottomTile = true;            
        }

        if (rightTile && leftTile && bottomTile)
        {
           return false;
        }
        if (!bottomTile)
        {
            //Debug.Log("There was no tile under this water: " + name);
            returnedPos.y -= 1;
            return true;
        }
        if (!rightTile)
        {
            returnedPos.x += 1;
        }
        else if(!leftTile)
        {
            returnedPos.x -= 1;
        }

        //Debug.Log(name);
        //Debug.Log(returnedPos);
        return true;
    }
}
