﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MagicBook
{
    private Magic currentMagic;

    public event EventHandler<MagicChangedArgs> MagicAddedEvent;
    public Dictionary<string, Magic> Pages { get; set; }

    public MagicBook()
    {
        Pages = new Dictionary<string, Magic>();
        var fireball = new Magic(MagicElement.Fire, MagicShape.Ball, "x1", "x1");
        var fireMani = new Magic(MagicElement.Water, MagicShape.Manipulate_Ball, "x1", "");
        var fireWall = new Magic(MagicElement.Earth, MagicShape.Manipulate_Ball, "x1", "");
        var fireWhirldl = new Magic(MagicElement.Fire, MagicShape.Whirl, "x1", "x1");
        AddPage(fireball);
        AddPage(fireMani);
        AddPage(fireWall);
        AddPage(fireWhirldl);
        CurrentMagic = Pages[fireball.Name];
    }

    public Magic AddPage(Magic page)
    {
        if (!Pages.ContainsKey(page.Name))
        {
            Pages.Add(page.Name, page);
            OnMagicChanged(this, new MagicChangedArgs("MagicBookChanged"));
            return page;
        }
        return null;
    }

    public bool RemovePage(Magic page)
    {
        if (!Pages.ContainsKey(page.Name))
        {
            Pages.Remove(page.Name);
            OnMagicChanged(this, new MagicChangedArgs("MagicBookChanged"));
            return true;
        }
        return false;
    }

    public Magic CurrentMagic
    {
        get { return currentMagic; }
        set
        {
            currentMagic = value;
            OnMagicChanged(currentMagic, new MagicChangedArgs("CurrentMagicChanged"));
        }
    }

    public int Count
    {
        get { return Pages.Count; }
    }
    private void OnMagicChanged(object obj, MagicChangedArgs e)
    {
        var tempMagicEvent = MagicAddedEvent;
        if (tempMagicEvent != null)
        {
            tempMagicEvent(obj,e);
        }
    }
    
}

