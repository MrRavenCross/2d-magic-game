﻿using UnityEngine;
using System.Collections;
public class Magic
{
    public string Name { get; set; }
    public int Damage { get; set; }
    public bool HasHit { get; set; }
    public MagicElement Element { get; set; }
    public MagicShape Shape { get; set; }
    /// <summary>
    /// Call this to get initial direction for the magic
    /// </summary>
    public string Direction { get; set; }
    /// <summary>
    /// Call this to get the second diretion. If this is the same as direction it will have double force.
    /// </summary>
    public string SecondDirection { get; set; }
    public float Force { get; private set; }
    /// <summary>
    /// Makes a new magic.
    /// </summary>
    /// <param name="element">The type of element the magic is</param>
    /// <param name="shape">The shape of the magic</param>
    /// <param name="direction">Is the starting direction of the magic</param>
    /// <param name="secondDirection">Is the second direction. When the magic hit it will go this direction.
    ///                               If the direction and second direction are the same, double force will be applied</param>
    public Magic(MagicElement element, MagicShape shape, string direction, string secondDirection)
    {
        HasHit = false;
        Element = element;
        Shape = shape;
        Direction = direction;
        SecondDirection = secondDirection;
        Force = 100;
        Name = Element.ToString() + Shape + FindDirectionName(direction, secondDirection);
        Damage = 10;
        determineForce();
    }
    /// <summary>
    /// Determines the force in the following order from the 2 Directions -> Shape -> Element. 
    /// </summary>
    private void determineForce()
    {
        float finalForce = 0;
        float x = 1;
        if (Direction == SecondDirection && Direction.Contains("0") && Direction != null && SecondDirection != null)
        {
            x = 2;
            HasHit = true;
        }
        if (string.IsNullOrEmpty(SecondDirection))
            HasHit = true;
        switch (Shape)
        {
            case MagicShape.Ball:
                break;
            case MagicShape.Whirl:
                finalForce *= 0.5f;
                break;
            case MagicShape.Wall:
                finalForce *= 0.25f;
                x = 1;
                break;
            case MagicShape.Manipulate_Ball:
                HasHit = true;
                break;
        }
        switch (Element)
        {
            case MagicElement.Air:
                Damage -= 5;
                finalForce = Force * x * 1.5f;
                break;
            case MagicElement.Earth:
                Damage += 5;
                finalForce = Force * x;
                break;
            case MagicElement.Water:
                Damage = 0;
                finalForce = Force * x * 1.25f;
                break;
            case MagicElement.Fire:
                Damage += 10;
                finalForce = Force * x * 1.35f;
                break;
        }
        
        Force = finalForce;
    }
    /// <summary>
    /// This is called when the name for the magic is being made.
    /// </summary>
    /// <returns>The combined name of the direction</returns>
    public static string FindDirectionName(string direction, string secondDirection)
    {
        string comboName = "";
        string[] directions = { direction, secondDirection };
        foreach (var dir in directions)
        {
            switch (dir)
            {
                case "x1":
                    comboName += "F";
                    break;
                case "x-1":
                    comboName += "B";
                    break;
                case "y1":
                    comboName += "U";
                    break;
                case "y-1":
                    comboName += "D";
                    break;
            }
        }
        return comboName;

    }
}
public enum MagicElement {Empty, Air = 1, Fire, Earth, Water }
public enum MagicShape {Empty, Ball = 1, Whirl, Wall, Manipulate_Ball }
