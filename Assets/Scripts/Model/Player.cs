﻿using System;
using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    public event EventHandler<HitPointArgs> HitPointsEventHandler;
    private int hitPoints;
    public MagicBook Averium { get; set; }

    public int HitPoints
    {
        get { return hitPoints; }
        set
        {
            hitPoints = value;
            OnHitPointsChanged(this, new HitPointArgs("HitPoints was changed",value));
        }
    }

    public string Name { get; set; }

    void Start()
    {
        HitPoints = 100;
        Averium = new MagicBook();
    }
    private void OnHitPointsChanged(object obj, HitPointArgs e)
    {
        var tempMagicEvent = HitPointsEventHandler;
        if (tempMagicEvent != null)
        {
            tempMagicEvent(obj, e);
        }
    }
}
